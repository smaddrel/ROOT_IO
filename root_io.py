import uproot
import matplotlib.pyplot as plt
import numpy as np
import argparse
from functools import wraps
import re

__author__ = "Sam Maddrell-Mander"
__copyright__ = "Copyright 20018, University of Bristol"
__version__ = "1.0.1"
__email__ = "sam.maddrell-mander@bristol.ac.uk"
__status__ = "Development"

'''
README:
- This is a lightweight alternative for basic display of ROOT files
- View histograms with basic functionality without the overhead of ROOT
- Package based on the UPROOT package (https://github.com/scikit-hep/uproot)
- Use case (1) quick and dirty visualisation
           (2) load module and call load_file(), then access self.dataframe
'''


class Plotter(object):
    """Docstring for Plotter."""

    hist_args = {'bins': 50, 'alpha': 0.8}

    def __init__(self, filename, branch=None, cut='', tree='DecayTree', **kwargs):
        super(Plotter, self).__init__()
        self.filename = filename
        self.branch = branch
        self.tree = tree
        self.cutstring = cut

    def load_file(self):
        while True:
            try:
                self.file = uproot.open(self.filename)[self.tree]
            except FileNotFoundError:
                print("Wrong file or file path")
                return -1
            else:
                try:
                    self.dataframe = self.file.pandas.df(self.file.keys())
                    keys = [key.decode('ASCII') for key in self.file.keys()]
                    self.dataframe.columns = keys
                except KeyError:
                    print("Key Error")
                    return -1
                return 1


    def inequalities(function):
        """
        Parse the cut string into dataframe readable format
        """
        @wraps(function)
        def wrapper(self,*args, **kwargs):
            s = self.cutstring.replace(' ','')
            subset = re.findall('\(.*?\)',s)
            print(subset)
            for sets in subset:
                print(sets)
                for c in sets:
                    if (c == '>'):
                        cut_type = 0;break
                    if (c == '<'):
                        cut_type = 1;break
                    if (c == '='):
                        cut_type = 2;break
                    char = c

                ineq_index = sets.index(char)
                name = sets[1:ineq_index+1]
                val = float(sets[ineq_index+2:-1])
                print(name, val)
                kwargs = {'name':name, 'cut_type':cut_type, "value":val}
                function(self, *args, **kwargs)
        return wrapper


    @inequalities
    def cuts(self, name, cut_type, value):
        """
        This is an example of where some meta programming would be really useful
        """
        if cut_type == 0:
            self.dataframe = self.dataframe[self.dataframe[name] > value]
        if cut_type == 1:
            self.dataframe = self.dataframe[self.dataframe[name] < value]
        if cut_type == 2:
            self.dataframe = self.dataframe[self.dataframe[name] == value]


    def decorated_plot(function):
        """
        Specifically trying to get limits in
        """
        @wraps(function)
        def wrapper(self,*args, **kwargs):
            xlim = np.percentile(np.hstack([self.dataframe[self.branch]]), [0.02, 98.0])
            return function(self,xlim=xlim,*args, **kwargs)

        return wrapper


    @decorated_plot
    def simple_plot(self,xlim, normed=False):
        plt.figure(figsize=(8,4))
        plt.title('{}, {}'.format(self.branch, self.cutstring), fontsize=8)
        plt.hist(self.dataframe[self.branch],range=xlim, density=normed, **self.hist_args)
        print(len(self.dataframe[self.branch]))
        plt.show()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some basic args.')
    parser.add_argument('--filename', help='Specify the filename to parse')
    parser.add_argument('--branch',   help='Specify the branch to plot')
    parser.add_argument('--normed',   action='store_true', default=False, help='normalise the histogram true/false')
    parser.add_argument('--cut',      default='',help='Specify the cut string, form: "(Bmass_rec > 5000) & (ctl_rec < 0.65)" ')
    args = parser.parse_args()
    args = (vars(args))
    P = Plotter(**args)
    success = P.load_file()
    if success > 0:
        if args['cut'] != '':
            P.cuts()
        print(args)
        P.simple_plot(normed=args['normed'])
